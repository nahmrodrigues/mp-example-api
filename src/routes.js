const express = require("express");
const router = express.Router();

// Controllers
const homeController = require("./controllers/homeController");
const mpCheckoutController = require("./controllers/mpCheckoutController");
const mpApiController = require("./controllers/mpApiController");

// Routes
router.get("/", homeController);

// Payment routes
router.post("/payment/mp-checkout", mpCheckoutController);
router.post("/payment/mp-api", mpApiController);

module.exports = router;

const mercadopago = require('mercadopago');

module.exports = (req, res) => {
    let preference = {
        items: [
            req.body
        ]
    };    
      
    mercadopago.preferences.create(preference)
        .then(function(response){
            let init_point = response.body.init_point;
            res.json(init_point);
        }).catch(function(error){
            res.json(error);
        });
};
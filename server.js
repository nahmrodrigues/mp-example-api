const http = require("http");
const app = require("./app");
const mongoose = require("mongoose");
const mercadopago = require("mercadopago");

// import environmental variables from our variables.env file
require("dotenv").config({ path: "variables.env" });

// Connect to our Database and handle any bad connections
mongoose.connect(process.env.DATABASE, {
    useNewUrlParser: true,
    useUnifiedTopology: true
});
mongoose.set("useCreateIndex", true);

// Tell Mongoose to use the default node.js promise implementation
mongoose.Promise = global.Promise;

mongoose.connection.on("error", err => {
    console.error(`💩 🚫 💩 🚫 💩 🚫 💩 🚫 → ${err.message}`);
});

mercadopago.configure({
    access_token: process.env.MP_ACCESS_TOKEN
});

const port = process.env.PORT || 8888;

const server = http.createServer(app);

server.listen(port);
